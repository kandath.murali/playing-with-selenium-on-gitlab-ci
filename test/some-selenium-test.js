const test = require('tape');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome')

const options  = new chrome.Options();
options.addArguments('headless');
options.addArguments('disable-gpu');
options.addArguments('disable-extensions');
options.addArguments('no-sandbox');
const capabilities = webdriver.Capabilities.chrome();
const driver = new webdriver.Builder()
  .forBrowser('chrome')
  .setChromeOptions(options)
  .withCapabilities(capabilities)
  .build();

test('Some selenium test', (t) => {
  t.plan(1);

  driver.get('http://192.168.22.230:8081/')
    .then(() => {
      driver.findElement(webdriver.By.xpath("//*[@id='__BVID__10']"));
    })
    .then(() => {
      t.pass('Successfully ran through selenium tasks');
    })
    .finally(() => {
      driver.quit();
    });
});
